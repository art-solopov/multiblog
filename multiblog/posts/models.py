from django.db import models
from django.contrib.auth.models import User

class RatingsManager(models.Manager):
    def get_queryset(self):
        qs = super().get_queryset()
        return qs.annotate(rating=models.Count('upvote')).order_by('-rating')

class Post(models.Model):
    text = models.TextField('Post')
    author = models.ForeignKey(User, on_delete=models.CASCADE)

    objects = models.Manager()
    objects_with_rating = RatingsManager()

class Attachment(models.Model):
    href = models.URLField(null=True, max_length=1000)
    attached_file = models.FileField(upload_to='attachments/%Y/%m/%d/')
    post = models.ForeignKey(Post, on_delete=models.CASCADE)

class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.TextField('Comment')

class Upvote(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)

    class Meta:
        unique_together = (('user', 'post'))
