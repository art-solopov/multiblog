from django.shortcuts import render
from django.views.generic import ListView
from posts.models import Post

class HomePageView(ListView):

    paginate_by = 10
    queryset = Post.objects_with_rating.all()
    context_object_name = 'posts'
